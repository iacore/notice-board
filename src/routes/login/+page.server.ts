import { error, redirect } from "@sveltejs/kit"
import { AuthProvider, createAdminToken } from "$lib/auth"
import type { Actions } from "./$types"

export const actions: Actions = {
  requestadmin: async (event) => {
    const token = await createAdminToken()
    const url = new URL(event.url)
    for (const key of url.searchParams.keys()) {
      url.searchParams.delete(key)
    }
    url.pathname = "/auth"
    url.searchParams.set("token", token)
    console.log(url.toString())
    return { success: true, seeConsole: true }
  },
  clearadmin: async (event) => {
    const auth = new AuthProvider(event.cookies)
    auth.clearAdmin()
    return { success: true }
  },
  getident: async (event) => {
    const auth = new AuthProvider(event.cookies)
    await auth.setNewUserId()
    throw redirect(301, '/login')
  },
}
