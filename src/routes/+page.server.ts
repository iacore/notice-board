import {
  error,
  fail,
  redirect,
  type Cookies,
  type ActionFailure,
} from "@sveltejs/kit"
import { PostSchema, getDb, type Post, Database } from "$lib/db"
import { z } from "zod"
import { AuthProvider } from "$lib/auth"

export const load: import("./$types").PageServerLoad = async function ({
  cookies,
}) {
  const auth = new AuthProvider(cookies)
  const [db, isAdmin, user_id] = await Promise.all([
    getDb(),
    auth.isAdmin(),
    auth.getUserId(),
  ])
  let posts = db.getPosts()
  if (!isAdmin)
    posts = posts.filter((p) => p.vetted || (user_id && p.createdBy == user_id))
  let posts2 = posts as Array<Post & { canEdit: boolean; canVet: boolean }>
  for (const p of posts2) {
    p.canEdit = await auth.canEditPost(p)
    p.canVet = await auth.canVetPost(p)
  }
  return {
    posts: posts2,
  }
}

const FormDataA = z.object({
  content: z.string().min(1),
})

const FormDataB = z.object({
  id: z.string(),
})

export const actions: import("./$types").Actions = {
  addpost: async ({ request, cookies }) => {
    const auth = new AuthProvider(cookies)
    const [db, user_id, unsafeData] = await Promise.all([
      getDb(),
      auth.getUserId(),
      request.formData(),
    ])
    if (!user_id) return fail(400, { message: "Not logged in" })
    const parsed = FormDataA.safeParse({
      content: unsafeData.get("content"),
    })
    if (!parsed.success) {
      const { fieldErrors } = parsed.error.formErrors
      return fail(400, { fieldErrors })
    }
    const post = parsed.data as Partial<Post>
    post.createdBy = user_id
    await db.addPost(post)
    throw redirect(301, "/")
  },
  deletepost: async ({ request, cookies }) => {
    const db = await getDb()
    const [res, err] = await dobedo({ db, request, cookies })
    if (err) return err
    const { auth, post } = res
    if (!post) return fail(400, { message: "no such post" })
    if (await auth.canEditPost(post)) {
      await db.deletePost(post.id)
      throw redirect(301, "/")
    } else {
      return fail(400, { message: "not authorized" })
    }
  },
  vetpost: async ({ request, cookies }) => {
    const db = await getDb()
    const [res, err] = await dobedo({ db, request, cookies })
    if (err) return err
    const { auth, post } = res
    if (!post) return fail(400, { message: "no such post" })
    if (await auth.canVetPost(post)) {
      await db.vetPost(post.id)
      throw redirect(301, "/")
    } else {
      return fail(400, { message: "not authorized" })
    }
  },
  unvetpost: async ({ request, cookies }) => {
    const db = await getDb()
    const [res, err] = await dobedo({ db, request, cookies })
    if (err) return err
    const { auth, post } = res
    if (!post) return fail(400, { message: "no such post" })
    if (await auth.canVetPost(post)) {
      await db.unvetPost(post.id)
      throw redirect(301, "/")
    } else {
      return fail(400, { message: "not authorized" })
    }
  },
}

async function dobedo({
  db,
  request,
  cookies,
}: {
  db: Database
  request: Request
  cookies: Cookies
}): Promise<
  | [
      undefined,
      ActionFailure<{
        fieldErrors: {
          id?: string[] | undefined
        }
      }>
    ]
  | [{ auth: AuthProvider; post: Post | undefined }, undefined]
> {
  const auth = new AuthProvider(cookies)
  const unsafeData = await request.formData()
  const parsed = FormDataB.safeParse({
    id: unsafeData.get("id"),
  })
  if (!parsed.success) {
    const { fieldErrors } = parsed.error.formErrors
    return [undefined, fail(400, { fieldErrors })]
  }
  const post = db.getPostById(parsed.data.id)
  return [{ auth, post }, undefined]
}
