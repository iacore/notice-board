import { error, redirect } from '@sveltejs/kit';
import { AuthProvider } from '$lib/auth'
 
export const GET: import('./$types').RequestHandler = async function ({ url, cookies }) {
    const token = new URL(url).searchParams.get("token")
    if (!token) throw error(400, { message: "no ?token=" })
    const auth = new AuthProvider(cookies)
    await auth.setAdmin(token)
    throw redirect(301, '/')
}
