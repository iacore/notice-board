import { AuthProvider } from "$lib/auth"
import type { LayoutServerLoad } from "./$types"

export const load: LayoutServerLoad = async function ({ cookies }) {
  const auth = new AuthProvider(cookies)
  return {
    isAdmin: await auth.isAdmin(),
    user: await auth.getUser(),
  }
}
