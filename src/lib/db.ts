// import fs from "fs/promises"

import { z } from "zod"
import { nanoid } from "nanoid"

export const PostSchema = z.object({
  id: z.string(), // nanoid
  createdBy: z.string(), // userid
  content: z.string().min(1),
  vetted: z.boolean(),
})

export type Post = z.infer<typeof PostSchema>

const DatabaseSchema = z.object({
  posts: z.array(PostSchema),
})

import { join, dirname } from "node:path"
import { fileURLToPath } from "node:url"
import { Low } from "lowdb"
// @ts-ignore
import { JSONFile } from "lowdb/node"

let singletonDb: Database | undefined

/** get singleton */
export async function getDb() {
  if (!singletonDb) {
    singletonDb = new Database()
    await singletonDb.init()
  }
  return singletonDb
}

export class Database {
  db: Low<z.infer<typeof DatabaseSchema>>
  constructor() {
    this.db = (function () {
      const file = "data/lowdb.json"
      const adapter = new JSONFile(file)
      return new Low(adapter)
    })()
  }

  async init() {
    await this.db.read()
    if (
      typeof this.db.data == "undefined" ||
      !DatabaseSchema.safeParse(this.db.data).success
    )
      this.db.data = { posts: [] }
  }

  getPosts(): Post[] {
    return this.db.data!.posts
  }

  async addPost(post0: Partial<Post>): Promise<string> {
    post0.id ??= nanoid()
    post0.vetted ??= false
    const post = PostSchema.parse(post0)
    this.getPosts().push(post)
    await this.db.write()
    return post.id
  }

  async deletePost(id: string) {
    const i = this.db.data!.posts.findIndex((p) => p.id == id)
    if (i < 0) return
    this.db.data!.posts.splice(i, 1)
    await this.db.write()
  }

  getPostById(id: string): Post | undefined {
    return this.db.data!.posts.find((p) => p.id == id)
  }

  async vetPost(id: string) {
    const i = this.db.data!.posts.findIndex((p) => p.id == id)
    if (i < 0) return
    this.db.data!.posts[i].vetted = true
    await this.db.write()
  }

  async unvetPost(id: string) {
    const i = this.db.data!.posts.findIndex((p) => p.id == id)
    if (i < 0) return
    this.db.data!.posts[i].vetted = false
    await this.db.write()
  }
}
