import type { Cookies } from "@sveltejs/kit"
import { nanoid } from "nanoid"
import { V4 as paseto, errors as pasetoErrors } from "paseto"
import fs from "fs/promises"
import type { Post } from "./db"
import type { KeyObject } from "crypto"

let pasetoKey: KeyObject

export async function createAdminToken() {
  const token = await signAssert({}, "IS_ADMIN", { expiresIn: "10m" })
  return token
}

export class AuthProvider {
  cookies: Cookies
  constructor(cookies: Cookies) {
    this.cookies = cookies
  }

  async setAdmin(token: string) {
    this.cookies.set("admin", token, { path: "/" })
  }

  clearAdmin() {
    this.cookies.delete("admin", { path: "/" })
  }

  async isAdmin(): Promise<boolean> {
    const token = this.cookies.get("admin")
    if (token == null) return false
    return Boolean(await verifyAssert(token, "IS_ADMIN"))
  }

  setNewUserId() {
    return this.setUserId(nanoid())
  }

  async setUserId(id: string) {
    const token = await signAssert({ id }, "USER", {
      expiresIn: "1 year",
    })
    this.cookies.set("user", token, { path: "/", maxAge: 60 * 60 * 24 * 365 })
  }

  async getUser(): Promise<{ id: string } | null> {
    const token = this.cookies.get("user")
    if (token == null) return null
    return (await verifyAssert(token, "USER")) as { id: string } | null
  }

  async getUserId(): Promise<string | null> {
    const user = await this.getUser()
    return user && user.id
  }

  async canEditPost(post: Post): Promise<boolean> {
    return (await this.isAdmin()) || (await this.getUserId()) == post.createdBy
  }

  async canVetPost(post: Post): Promise<boolean> {
    return await this.isAdmin()
  }
}

type PasetoPayload = Record<PropertyKey, unknown>

async function getPasetoKey() {
  if (!pasetoKey)
    pasetoKey = await (async function () {
      try {
        return paseto.bytesToKeyObject(await fs.readFile("data/pasetokey"))
      } catch (e) {
        console.error("Try generate key with: node scripts/init.js")
        throw e
      }
    })()
  return pasetoKey
}

async function signAssert(
  data: PasetoPayload,
  assertion: string,
  opts: Partial<{ expiresIn: string }>
): Promise<string> {
  const token = await paseto.sign(data, await getPasetoKey(), {
    assertion: assertion,
    expiresIn: opts.expiresIn,
  })
  return token
}

async function verifyAssert(
  token: string,
  assertion: string
): Promise<PasetoPayload | null> {
  try {
    return await paseto.verify(token, await getPasetoKey(), {
      assertion: assertion,
    })
  } catch (e) {
    if (e instanceof pasetoErrors.PasetoVerificationFailed) {
    } else if (e instanceof pasetoErrors.PasetoInvalid) {
    } else if (e instanceof pasetoErrors.PasetoClaimInvalid) {
    } else {
      console.error(e)
    }
    return null
  }
}
