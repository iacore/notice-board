/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/app.html",
    "./src/**/*.{js,ts,jsx,tsx,svelte,svx}",
  ],
  corePlugins: {
    preflight: false
  },
  theme: {
    extend: {},
  },
  plugins: [],
}
