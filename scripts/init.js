#!/usr/bin/env node

import { V4 as paseto } from "paseto"
import fs from "fs/promises"
const pasetoKey = await paseto.generateKey("public")

await fs.mkdir("data", { recursive: true })
await fs.writeFile("data/pasetokey", paseto.keyObjectToBytes(pasetoKey))
